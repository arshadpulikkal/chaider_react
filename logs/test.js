let data = {
  parentId: "1122334",
  parentName: "test",
  childInfo: [
    {
      childId: "1223",
      childName: "abc",
      parentId: "12213",
      parentName: "test",
      childDescrption: "abcd"
    },
    {
      childId: "1225",
      childName: "abcd",
      parentId: "12213",
      parentName: "test",
      childDescrption: "abcddfgdf"
    },
    {
      childId: "122358",
      childName: "abrtc5",
      parentId: "12213",
      parentName: "test",
      childDescrption: "sdfabcd"
    },
    {
      childId: "12257",
      childName: "abcfgd",
      parentId: "12213",
      parentName: "test",
      childDescrption: "abcddfgdf"
    },
    {
      childId: "122356",
      childName: "rtabc5",
      parentId: "12213",
      parentName: "test",
      childDescrption: "sdfabcd"
    }
  ]
};

let sampleGraphData = `1("<div style='background: #6cb33e; color: #ffffff'><b>Spring PetClinic</div>")
                2("<div id = title><b>Clinic Employee</b><br /><sup>[Person]</sup><br />An employee of the clinic</div>")
                style 2 fill:#519823
                style 1 fill:#6cb33e
                2-->1
                b-->a
                a-->${data.parentId}`;

                  //  data.childInfo.map(i => {
      //    let l = i.childName.split(" ")
      //    console.log(l[0])
      //  })