import Mermaid from "../../flowChart/flowChart";
import React from "react";

function FlowChartData() {
  window.home_tab_fetchMore = function() {};
  return (
    <div>
      <Mermaid
        className="flowChart"
        chart={`graph TD;
      %% DEFINING CUSTOMER FLOW-CHART SHAPES AND CONTENT

      PARENTBLOCK("<div id = All>All customers, <br> <br> Response Rate = </div>")
      CHILDBLOCK1("<div id = Rural>Rural <br> <br> 18.6%</div>")
      CHILDBLOCK2("<div id = Urban>Urban <br> <br> 28.5% </div>")
      SUBCHILDBLOCKRURALFT("<div id = Full>Full Time <br> <br> 24.0% </div>")
      SUBCHILDBLOCKRURALPT("<div id = Part>Part Time <br> <br> 17.8% </div>")
      SUBCHILDBLOCKRURALR/UNEMP("<div id = Retired>Retired <br> <br> 5.3% </div>")
      SUBCHILDBLOCKURBANHOMEOWN("<div id = Homeowner>Homeowner <br> <br> 36.1% </div>")
      SUBCHILDBLOCKURBANNONHOMEOWN("<div id = Non>Non Homeowner <br> <br> 22.7% </div>")
      ENDBLOCKSAGE18-64("<div id = Aged18>Aged18 - 64 <br> <br> 10.7% </div>")
      ENDBLOCKSAGE65("<div id = Aged>Aged > 65,Aged > 69. <br> <br> 1.4% </div>")

      %% CREATE "CUSTOMER" FLOW CHART

      PARENTBLOCK === CHILDBLOCK1
      PARENTBLOCK === CHILDBLOCK2
      CHILDBLOCK1 === SUBCHILDBLOCKRURALFT
      CHILDBLOCK1 === SUBCHILDBLOCKRURALPT
      CHILDBLOCK1 === SUBCHILDBLOCKRURALR/UNEMP
      CHILDBLOCK2 === SUBCHILDBLOCKURBANHOMEOWN
      CHILDBLOCK2 === SUBCHILDBLOCKURBANNONHOMEOWN
      SUBCHILDBLOCKRURALR/UNEMP === ENDBLOCKSAGE18-64
      SUBCHILDBLOCKRURALR/UNEMP === ENDBLOCKSAGE65
      `}
      />
    </div>
  );
}

export default FlowChartData;



