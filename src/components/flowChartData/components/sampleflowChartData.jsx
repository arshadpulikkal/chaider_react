import React, { Component } from "react";
import "../styles/style.css";
import mermaid from "mermaid";
import debounce from "debounce";
import FlowChartData from "./sampleflowChartData copy";
import { Link } from "react-router-dom";
import "../../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { Form, InputGroup, Toast, Button } from "react-bootstrap";
import $ from "jquery";
import axios from "axios";
import swal from "@sweetalert/with-react";
import parentBlockData from "../../requestData/parentBlockData";
import requestData from "../../requestData/requestData.json";
import SavedFlowChart from "../../savedFlowChart/savedFlowChart";
import Header from "../../header/header";

let flowChartData = parentBlockData;
class SampleFlowChartData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ruleValue: "Urban",
      showRuleDropDown: false,
      flowchartValue: "",
      showSaveTab: false,
      flowChartHtmlData: flowChartData,
      showFlowChart: false,
      showSavedFlowChartModel: false,
      flowChartJsonData: "",
      flowChartDivIndex: ""
    };
  }

  async componentDidMount() {
    let response = await axios.get(
      "http://3.132.216.117:8000/chaider/api/v1/tree"
    );
    this.setState({ flowChartJsonData: response.data });
    console.log(this.state.flowChartJsonData);
  }

  handleDivIdClik = async divId => {
    $(divId).on("click", event => {
      this.setState({ showRuleDropDown: true });
      console.log("hello", divId);
      this.setState({ flowChartDivIndex: divId.replace("#", "") });
    });
  };

  handleCreateDropDown = e => {
    let ruleValue = e.target.name;
    this.setState({ [ruleValue]: e.target.value });
  };

  handleSavedFlowChart = () => {
    this.setState({ showSavedFlowChartModel: true });
  };

  handleSubmit = async () => {
    try {
      let response = await axios.get(
        "http://3.132.216.117:8000/chaider/api/v1/tree",
        {
          params: {
            key: this.state.flowChartJsonData.childInfo[
              this.state.flowChartDivIndex
            ]
          }
        }
      );
      console.log(response)
      this.postJsonData();
      this.state.flowChartJsonData.childInfo.push(response);
      console.log(
        this.state.flowChartJsonData.childInfo[this.state.flowChartDivIndex]
      );
      response.data.childInfo.map((chartValue, i) => {
        let graph = `

        ${chartValue.childId}("<div id = ${i.toString()}>${
          chartValue.childName
        }</div>")
        
        ${chartValue.parentId} === ${chartValue.childId}
        `;
        flowChartData = this.state.flowChartHtmlData + "\n" + graph;
        console.log("\n" + graph);
        this.setState({ showRuleDropDown: false });
        this.setState({ flowChartHtmlData: flowChartData });
        this.handleCreate();
      });
    } catch (err) {
      console.log(err);
    }
  };

  postJsonData = async () => {
    try {
      await axios.post(
        "http://3.132.216.117:8000/chaider/api/v1/tree",
        this.state.flowChartJsonData
      );
      console.log({ data: this.state.flowChartJsonData });
      console.log("posted successfully");
    } catch (err) {
      console.log(err);
    }
  };

  handleCreate = debounce(
    data => {
      console.log(`${this.state.flowChartHtmlData}`);
      this.setState({ showFlowChart: true });
      var output = document.getElementById("flowChart");
      try {
        mermaid.parse(this.state.flowChartHtmlData);

        mermaid.render("theGraph", this.state.flowChartHtmlData, function(
          svgCode
        ) {
          output.innerHTML = svgCode;
        });
      } catch (err) {
        console.error(err);
      }
      $(document).click(event => {
        const text = $(event.target).text();
        const id = `#${event.target.id}`;
        if (id.length === 2) {
          this.handleDivIdClik(id);
        } else {
          return;
        }
      });
    },
    600,
    false
  );

  render() {
    if (this.state.showRuleDropDown === true) {
      swal({
        buttons: "Cancel",
        content: (
          <div className="popUp">
            <Form>
              <Form.Group controlId="exampleForm.ControlSelect1">
                <Form.Label>Rule</Form.Label>
                <InputGroup className="mb-3">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon3">Rule</InputGroup.Text>
                  </InputGroup.Prepend>
                  <Form.Control
                    as="select"
                    name="ruleValue"
                    defaultValue={this.state.ruleValue}
                    onChange={this.handleCreateDropDown}
                  >
                    {requestData.childInfo.map(rule => {
                      return <option>{rule.rule}</option>;
                    })}
                  </Form.Control>
                </InputGroup>
              </Form.Group>
            </Form>
            <Button
              className="popupButton"
              onClick={() => {
                this.handleSubmit();
                swal.close();
              }}
            >
              Ok
            </Button>
          </div>
        )
      }).then(() => {
        swal.close();
      });
    }

    if (this.state.showSavedChartModel === true) {
      return (
        <div>
          <Header />
          <div id="flowChart"></div>
        </div>
      );
    }
    const saveTab = (
      <div>
        <Toast
          style={{ width: "20rem" }}
          onClose={() => this.setState({ showSaveTab: false })}
          className="toast"
        >
          <Toast.Header>
            <img
              src="holder.js/20x20?text=%20"
              className="rounded mr-2"
              alt=""
            />
            <strong className="mr-auto" onClick={this.handleSavedFlowChart}>
              <Link
                to={{
                  pathname: "/savedFlowChart",
                  data: {
                    flowChartData: this.state.flowChartHtmlData,
                    flowChartJsonData: this.state.flowChartJsonData
                  }
                }}
              >
                {" "}
                {this.state.flowChartJsonData.documentId}
              </Link>
            </strong>
            <small>2 seconds ago</small>
          </Toast.Header>
          <Toast.Body>{this.state.flowChartJsonData.Descrption}</Toast.Body>
        </Toast>
        <div id="flowChart"></div>
      </div>
    );

    if (this.state.showSaveTab === true) {
      return (
        <div>
          <Header />
          <div>{saveTab}</div>
        </div>
      );
    } else if (this.state.showFlowChart === false) {
      return (
        <div>
          <Header />
          <Button
            variant="outline-info"
            className="createChartButton"
            onClick={this.handleSubmit}
          >
            Create FlowChart
          </Button>
        </div>
      );
    } else {
      return (
        <div>
          <Header />
          <div id="flowChart"></div>
          <Button
            className="flowChartSaveButton"
            variant="outline-dark"
            onClick={() => this.setState({ showSaveTab: true })}
          >
            Save chart
          </Button>
        </div>
      );
    }
  }
}

export default SampleFlowChartData;
