import React, { Component } from "react";
import mermaid from "mermaid";
import Header from "../header/header";
import "./savedFlowChart.css";
import axios from "axios";
import parentBlockData from "../requestData/parentBlockData";
import debounce from "debounce";

let flowChartData = parentBlockData;
class SavedFlowChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flowChartHtmlData: parentBlockData,
      flowChartJsonData: ""
    };
  }

  async componentDidMount() {
    const response = await this.postFlowChartJsonData();
    // console.log(response.data.tree);
    response.data.tree.map((chartValue, i) => {
      console.log(chartValue)
      let graph = `

      ${chartValue.childId}("<div id = ${i.toString()}>${
        chartValue.childName
      }</div>")
      
      ${chartValue.parentId} === ${chartValue.childId}
      `;
      flowChartData = this.state.flowChartHtmlData + "\n" + graph;
      console.log("\n" + graph);
      this.setState({ showRuleDropDown: false });
      this.setState({ flowChartHtmlData: flowChartData });
      this.handleCreate();
    });
  }
  catch(err) {
    console.log(err);
  }

  postFlowChartJsonData = async () => {
    try {
      const response = await axios.get(
        "http://3.132.216.117:8000/chaider/api/v1/tree?_id=all"
      );
      return response;
    } catch (err) {
      console.log(err);
    }
  };

  handleCreate = debounce(
    () => {
      // console.log(`${this.state.flowChartHtmlData}`);
      var output = document.getElementById("flowChart");
      try {
        mermaid.parse(this.state.flowChartHtmlData);

        mermaid.render("theGraph", this.state.flowChartHtmlData, function(
          svgCode
        ) {
          output.innerHTML = svgCode;
        });
      } catch (err) {
        console.error(err);
      }
    },
    600,
    false
  );

  render() {
    return (
      <div className="savedChartDisplay">
        <Header />
        <div id="flowChart"></div>
      </div>
    );
  }
}

export default SavedFlowChart;
