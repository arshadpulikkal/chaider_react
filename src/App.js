import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import SampleFlowChartData from "./components/flowChartData/components/sampleflowChartData";
import SavedFlowChart from "./components/savedFlowChart/savedFlowChart";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact component={SampleFlowChartData} />
          <Route path="/savedFlowChart" component={SavedFlowChart} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
