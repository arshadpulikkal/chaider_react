import React from "react";
import "./App.css";
import SampleFlowChartData from "./components/sampleflowChartData"

function App() {
  return (  
    <div className="App">
    <SampleFlowChartData/>
    </div>
  );
}

export default App;


